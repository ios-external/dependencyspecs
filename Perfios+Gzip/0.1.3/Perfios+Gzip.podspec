Pod::Spec.new do |spec|
    spec.name                   = "Perfios+Gzip"
    spec.version                = "0.1.3"
    spec.summary                = "Bitcode Enabled Gzip framework for Perfios Software Solutions Private Ltd."
    spec.description            = <<-DESC
    Perfios+BigInt is a framework extention of BigIntSwift by 1024jp which is bitcode enabled for development by Perfios Software Solutions Private Ltd.
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/perfios-gzip-framework.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@perfios.com" }
    spec.platform               = :ios, "10.0"
    spec.ios.deployment_target  = "10.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/perfios-gzip-framework.git", :tag => "#{spec.version}" }
    spec.vendored_frameworks    = "Perfios_Gzip.xcframework"
  end
