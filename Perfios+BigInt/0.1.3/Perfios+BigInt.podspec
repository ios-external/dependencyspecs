Pod::Spec.new do |spec|
  spec.name                   = "Perfios+BigInt"
  spec.version                = "0.1.3"
  spec.summary                = "Bitcode Enabled BigInt framework for Perfios Software Solutions Private Ltd."
  spec.description            = <<-DESC
    Perfios+BigInt is a framework extention of BigInt by AttaSwift which is bitcode enabled for development by Perfios Software Solutions Private Ltd.
  DESC
  spec.homepage               = "https://bitbucket.org/ios-external/perfios-bigint-framework.git"
  spec.license                = { :type => "MIT", :file => "LICENSE" }
  spec.author                 = { "Sanjay Kumawat" => "sanjay.k@perfios.com" }
  spec.platform               = :ios, "10.0"
  spec.ios.deployment_target  = "10.0"
  spec.source                 = { :git => "https://bitbucket.org/ios-external/perfios-bigint-framework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks    = "Perfios_BigInt.xcframework"
end
