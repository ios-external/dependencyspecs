Pod::Spec.new do |spec|
    spec.name                   = "Karza+Gzip"
    spec.version                = "0.1.3"
    spec.summary                = "Bitcode Enabled Gzip framework for Karza Technologies"
    spec.description            = <<-DESC
        Karza+BigInt is a framework extention of BigIntSwift by 1024jp which is bitcode enabled for development by Karza Technologies
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/karza-gzip-framework.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@karza.in" }
    spec.platform               = :ios, "10.0"
    spec.ios.deployment_target  = "10.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/karza-gzip-framework.git", :tag => "#{spec.version}" }
    spec.vendored_frameworks    = "Karza_Gzip.xcframework"
  end
