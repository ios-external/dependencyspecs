Pod::Spec.new do |spec|
    spec.name                   = "Perfios+Lottie"
    spec.version                = "0.1.0"
    spec.summary                = "Bitcode Enabled Lottie framework for Perfios Software Solutions Private Ltd."
    spec.description            = <<-DESC
    Perfios+Lottie is a framework extention of Lottie by airbnb which is bitcode enabled for development by Perfios Software Solutions Private Ltd.
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/perfios-lottie.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@perfios.com" }  
    spec.platform               = :ios, "11.0"
    spec.ios.deployment_target  = "11.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/perfios-lottie.git", :tag => "#{spec.version}" }
    spec.vendored_frameworks    = "Perfios_Lottie.xcframework"
  end
  