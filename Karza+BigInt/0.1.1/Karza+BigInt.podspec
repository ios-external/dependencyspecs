Pod::Spec.new do |spec|
    spec.name                   = "Karza+BigInt"
    spec.version                = "0.1.1"
    spec.summary                = "Bitcode Enabled BigInt framework for Karza Technologies"
    spec.description            = <<-DESC
        Karza+BigInt is a framework extention of BigInt by AttaSwift which is bitcode enabled for development by Karza Technologies
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/karza-bigint-framework.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@karza.in" }  
    spec.platform               = :ios, "10.0"
    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.ios.deployment_target  = "10.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/karza-bigint-framework.git", :tag => "#{spec.version}" }
    spec.exclude_files          = "Classes/Exclude"
    spec.public_header_files    = "BigInt.framework/Headers/*.h"
    spec.source_files           = "BigInt.framework/Headers/*.h"
    spec.vendored_frameworks    = "BigInt.framework"
  end
  
