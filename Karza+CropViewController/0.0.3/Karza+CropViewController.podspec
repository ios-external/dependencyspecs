Pod::Spec.new do |spec|
  spec.name                   = "Karza+CropViewController"
  spec.version                = "0.0.3"
  spec.summary                = "Bitcode enabled version of TOCropViewController for use by Karza Technologies"
  spec.description            = <<-DESC
  TOCropViewController is an open-source UIViewController subclass to crop out sections of UIImage objects, as well as perform basic rotations. It is excellent for things like editing profile pictures, or sharing parts of a photo online. It has been designed with the iOS Photos app editor in mind, and as such, behaves in a way that should already feel familiar to users of iOS.
  DESC
  spec.homepage               = "https://bitbucket.org/ios-external/karzacropviewcontroller.git"
  spec.license                = { :type => "MIT", :file => "LICENSE" }
  spec.author                 = { "Sanjay Kumawat" => "sanjay.k@karza.in" }
  spec.platform               = :ios, "10.0"
  spec.ios.deployment_target  = "10.0"
  spec.source                 = { :git => "https://bitbucket.org/ios-external/karzacropviewcontroller.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks    = "CropViewController.xcframework"
end
