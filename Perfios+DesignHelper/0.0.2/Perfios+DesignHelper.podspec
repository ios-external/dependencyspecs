Pod::Spec.new do |spec|
    spec.name                   = "Perfios+DesignHelper"
    spec.version                = "0.0.2"
    spec.summary                = "Perfios Design Helper framework for Perfios Software Solutions Private Ltd."
    spec.description            = <<-DESC
        Perfios+Login is a framework for helping with UI designing for development by Perfios Software Solutions Private Ltd.
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/ios_design_helper_framework.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@perfios.com" }
    spec.platform               = :ios, "12.0"
    spec.ios.deployment_target  = "12.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/ios_design_helper_framework.git", :tag => "#{spec.version}" }
    spec.vendored_frameworks    = "DesignHelper.xcframework"
  end
