Pod::Spec.new do |spec|
    spec.name                   = "Perfios+Login"
    spec.version                = "0.0.1"
    spec.summary                = "Bitcode Enabled Perfios Login framework for Perfios Software Solutions Private Ltd."
    spec.description            = <<-DESC
        Perfios+Login is a framework for tkyc login for development by Perfios Software Solutions Private Ltd.
    DESC
    spec.homepage               = "https://bitbucket.org/ios-external/ios_karza_login_framework.git"
    spec.license                = { :type => "MIT", :file => "LICENSE" }
    spec.author                 = { "Sanjay Kumawat" => "sanjay.k@perfios.com" }
    spec.platform               = :ios, "12.0"
    spec.ios.deployment_target  = "12.0"
    spec.source                 = { :git => "https://bitbucket.org/ios-external/ios_karza_login_framework.git", :tag => "#{spec.version}" }
    spec.vendored_frameworks    = "Perfios_Login.xcframework"
    spec.dependency "Perfios+DesignHelper"
  end